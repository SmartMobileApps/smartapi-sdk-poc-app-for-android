#SmartAPI SDK POC App for Android
* * *
This Android App utilizes the *SmartAPI SDK* to create and parse responses using Java objects.

**NOTE: the SDK provided only connects to the staging servers**

Check the following snippets below

##Setup
Add the SDK jar into your library folder and To include the Smart API SDK in your project, just copy the jar file to any path that is visible to your classes. For Android, place it in your libs folder and add that jar to your classpath (or add it as a dependency if you are using Gradle)

##Pin Consent Request
####Creating the Request
* DetailActivity.java (108-112)

		PinConsentRequest pcr = new PinConsentRequest.Builder("CLIENT_ID", "CLIENT_SECRET")
				.setClientReferenceNumber("00000")
				.setScope("chargeAmountAirtime")
				.setSubscriberMsisdn(new Msisdn(App.getSubscriber()))
				.build();

####Parsing the response
* API.java (94)

		listener.onSuccess(new ChargeResponse(response.toJSONObject()));


##Charge Request
####Creating the Request
* DetailActivity.java (166-168)

		ChargeRequest cr = new ChargeRequest.Builder("CLIENT_ID", pinEntry.getText().toString(), new Msisdn(App.getSubscriber()))
				.setParams("MERCHANT_ID", "00000", "SERVICE_ID", ResourceState.Amount.STATE_CHARGED)
				.build();

####Parsing the response
* API.java (93-96)

		if (response.isSuccess()) {
			listener.onSuccess(new ChargeResponse(response.toJSONObject()));
		} else {
			listener.onFailure(new BillingError(response.toJSONObject()));
		}