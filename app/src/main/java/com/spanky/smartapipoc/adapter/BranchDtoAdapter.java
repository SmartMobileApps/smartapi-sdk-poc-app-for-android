package com.spanky.smartapipoc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spanky.smartapipoc.R;
import com.spanky.smartapipoc.activity.DetailActivity;
import com.spanky.smartapipoc.dto.BranchDto;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by Viel on 9/24/2015.
 */
public class BranchDtoAdapter extends RecyclerView.Adapter<BranchDtoAdapter.ViewHolder> {

    private List<BranchDto> branchDtoList;
    private Context context;

    public BranchDtoAdapter(Context context) {
        branchDtoList = new LinkedList<>();
        this.context = context;
    }

    public void setBranchDtoList(List<BranchDto> branchDtoList) {
        this.branchDtoList = branchDtoList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvBranchName)
        AppCompatTextView tvBranchName;
        @Bind(R.id.tvAddress)
        AppCompatTextView tvAddress;
        @Bind(R.id.tvDistance)
        AppCompatTextView tvDistance;
        View view;
        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public BranchDtoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_branches, parent, false));
    }

    @Override
    public void onBindViewHolder(BranchDtoAdapter.ViewHolder holder, int position) {
        final BranchDto current = branchDtoList.get(position);
        holder.tvAddress.setText(current.getAddress());
        holder.tvBranchName.setText(current.getBranchName());
        holder.tvDistance.setText(String.format("%.0f\nmtrs", current.getDist()));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailActivity.class));
                EventBus.getDefault().postSticky(current);
            }
        });
    }

    @Override
    public int getItemCount() {
        return branchDtoList.size();
    }


}
