package com.spanky.smartapipoc.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.spanky.smartapipoc.R;
import com.spanky.smartapipoc.activity.DetailActivity;
import com.spanky.smartapipoc.dto.Promo;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by Viel on 9/28/2015.
 */
public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.ViewHolder> {

    private Context context;
    private List<Promo> promos = new LinkedList<>();
    private static final String DRAWABLE_PREFIX = "android.resource://com.spanky.smartapipoc/drawable/";
    private DetailActivity.RefreshOptionsMenuListener listener;
    private int selectedCount;

    public PromoAdapter(Context context, DetailActivity.RefreshOptionsMenuListener listener) {
        this.context = context;
        this.listener = listener;
        promos.add(new Promo("Deezer Daily", 25f, Uri.parse(DRAWABLE_PREFIX + R.drawable.promo_1).toString()));
        promos.add(new Promo("Big Bytes", 99f, Uri.parse(DRAWABLE_PREFIX + R.drawable.promo_2).toString()));
        promos.add(new Promo("Trio Talk Plus", 149f, Uri.parse(DRAWABLE_PREFIX + R.drawable.promo_3).toString()));
        promos.add(new Promo("International Flexi Call & Text", 50f, Uri.parse(DRAWABLE_PREFIX + R.drawable.promo_4).toString()));
        promos.add(new Promo("All Text", 200f, Uri.parse(DRAWABLE_PREFIX + R.drawable.promo_5).toString()));
        Collections.shuffle(promos);
        notifyDataSetChanged();
    }

    public void getPromos() {
        EventBus.getDefault().post(promos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ivLogo)
        ImageView ivLogo;
        @Bind(R.id.tvName)
        AppCompatTextView tvName;
        @Bind(R.id.tvPrice)
        AppCompatTextView tvPrice;
        @Bind(R.id.indicator)
        View indicator;
        View view;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_promo, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Promo currentItem = promos.get(position);
        Picasso.with(context).load(currentItem.getResourcePath()).into(holder.ivLogo);
        holder.tvName.setText(currentItem.getName());
        holder.tvPrice.setText(String.format("PHP %.0f", currentItem.getPrice()));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentItem.setSelected(!currentItem.isSelected());
                holder.indicator.setVisibility(currentItem.isSelected() ? View.VISIBLE : View.GONE);
                selectedCount = currentItem.isSelected() ? selectedCount + 1 : selectedCount - 1;
                listener.refresh(selectedCount > 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return promos.size();
    }
}
