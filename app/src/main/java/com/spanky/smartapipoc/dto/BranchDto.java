package com.spanky.smartapipoc.dto;

import com.truebanana.json.JSONUtils;

import org.json.JSONObject;

/**
 * Created by Viel on 9/23/2015.
 */
public class BranchDto {

    private float lat;
    private float lng;
    private float dist;
    private String branchName;
    private String address;

    public BranchDto(JSONObject branchObject) {
        this.lat = JSONUtils.getFloat(branchObject, "latitude");
        this.lng = JSONUtils.getFloat(branchObject, "longitude");
        this.branchName = JSONUtils.getString(branchObject, "branchName");
        this.address = JSONUtils.getString(branchObject, "address");
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }

    public float getDist() {
        return dist;
    }

    public void setDist(float dist) {
        this.dist = dist;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getAddress() {
        return address;
    }
}
