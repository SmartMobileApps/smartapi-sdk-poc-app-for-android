package com.spanky.smartapipoc.dto;

/**
 * Created by Viel on 9/28/2015.
 */
public class Promo {

    private String name;
    private float price;
    private String resourcePath;
    private boolean selected;

    public Promo(String name, float price, String resourcePath) {
        this.name = name;
        this.price = price;
        this.resourcePath = resourcePath;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
