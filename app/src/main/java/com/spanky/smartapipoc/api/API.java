package com.spanky.smartapipoc.api;

import com.spanky.smartapipoc.dto.BranchDto;
import com.truebanana.http.BasicResponseListener;
import com.truebanana.http.HTTPRequest;
import com.truebanana.http.HTTPRequestError;
import com.truebanana.http.HTTPResponse;
import com.truebanana.toast.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ph.com.smart.consentSDK.billing.BillingError;
import ph.com.smart.consentSDK.billing.ChargeRequest;
import ph.com.smart.consentSDK.billing.ChargeResponse;
import ph.com.smart.consentSDK.consent.ConsentResponse;
import ph.com.smart.consentSDK.consent.PinConsentRequest;

/**
 * Created by Viel on 9/23/2015.
 */
public class API {

    public static void getBranches(final GetBranchesSuccessListener listener) {
        HTTPRequest.create("https://apis.smart.com.ph:7443/infinity-v2/branchManagement/branches")
                .setGet()
                .addHeader("Authorization", "Basic aW5maW5pdHk6aW5maW5pdHk=")
                .setHTTPResponseListener(new BasicResponseListener() {
                    @Override
                    public void onRequestCompleted(HTTPResponse response) {
                        List<BranchDto> retval = new LinkedList<>();
                        super.onRequestCompleted(response);
                        JSONArray responseArray = response.toJSONArray();
                        for (int i = 0; i < responseArray.length(); i++) {
                            try {
                                retval.add(new BranchDto(responseArray.getJSONObject(i)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        listener.onSuccess(retval);
                    }

                    @Override
                    public void onRequestError(HTTPRequestError error) {
                        super.onRequestError(error);
                    }
                })
                .executeAsync();
    }

    public interface GetBranchesSuccessListener {
        void onSuccess(List<BranchDto> branchDtos);
    }

    public static void sendPinConsent(PinConsentRequest pinConsentRequest, final SendPinConsentListener listener) {
        HTTPRequest request = HTTPRequest.create(pinConsentRequest.getUrl().toString())
                .setPost()
                .setRequestBody(pinConsentRequest.toJson())
                .setLogTag("Pin consent request")
                .setSSLVerificationEnabled(false)
                .setHTTPResponseListener(new BasicResponseListener() {
                    @Override
                    public void onRequestCompleted(HTTPResponse response) {
                        super.onRequestCompleted(response);
                        listener.onSuccess(new ConsentResponse(response.toJSONObject()));
                    }
                });
        for (Map.Entry<String,String> header : pinConsentRequest.getRequestHeaders().entrySet()) {
            request.addHeader(header.getKey(), header.getValue());
        }
        request.executeAsync();
    }

    public interface SendPinConsentListener {
        void onSuccess(ConsentResponse consentResponse);
    }

    public static void sendChargeRequest(ChargeRequest chargeRequest, final SendChargeRequestListener listener) {
        HTTPRequest request = HTTPRequest.create(chargeRequest.getUrl().toString())
                .setPost()
                .setRequestBody(chargeRequest.toUrlEncoded())
                .setLogTag("Charge request")
                .setSSLVerificationEnabled(false)
                .setHTTPResponseListener(new BasicResponseListener() {
                    @Override
                    public void onRequestCompleted(HTTPResponse response) {
                        super.onRequestCompleted(response);
                        if (response.isSuccess()) {
                            listener.onSuccess(new ChargeResponse(response.toJSONObject()));
                        } else {
                            listener.onFailure(new BillingError(response.toJSONObject()));
                        }
                    }
                });
        for (Map.Entry<String,String> header : chargeRequest.getRequestHeaders().entrySet()) {
            request.addHeader(header.getKey(), header.getValue());
        }
        request.executeAsync();

    }

    public interface SendChargeRequestListener {
        void onSuccess(ChargeResponse response);
        void onFailure(BillingError error);
    }

}
