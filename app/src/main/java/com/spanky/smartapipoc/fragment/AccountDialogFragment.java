package com.spanky.smartapipoc.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.spanky.smartapipoc.App;
import com.spanky.smartapipoc.R;
import com.truebanana.toast.Toast;

import ph.com.smart.consentSDK.utils.Msisdn;

/**
 * Created by Viel on 9/28/2015.
 */
public class AccountDialogFragment extends DialogFragment {

    public static AccountDialogFragment newInstance() {
        AccountDialogFragment adf = new AccountDialogFragment();
        return adf;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_account, null);
        final EditText number = (EditText) dialogView.findViewById(R.id.etMsisdn);
        if (App.getSubscriber() != null) {
            number.setText(App.getSubscriber());
        }
        return new AlertDialog.Builder(getActivity())
                .setTitle("My Account")
                .setView(dialogView)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            App.saveSubscriber(new Msisdn(number.getText().toString()));
                        } catch (IllegalArgumentException e) {
                            Toast.show(getActivity(), "Invalid phone number");
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if(d != null) {
            d.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }
}
