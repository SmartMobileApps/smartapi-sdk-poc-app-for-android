package com.spanky.smartapipoc.activity;

import android.app.ProgressDialog;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.spanky.smartapipoc.R;
import com.spanky.smartapipoc.adapter.BranchDtoAdapter;
import com.spanky.smartapipoc.api.API;
import com.spanky.smartapipoc.dto.BranchDto;
import com.spanky.smartapipoc.fragment.AccountDialogFragment;
import com.truebanana.Utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {


    //    @Bind(R.id.rvBranches)
//    RecyclerView rvBranches;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.rvBranches)
    RecyclerView rvBranches;
    @Bind(R.id.fabRefresh)
    FloatingActionButton fabRefresh;
    BranchDtoAdapter adapter;

    private Location myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initToolbar(null);
        collapsingToolbarLayout.setTitle("Smappi");
        rvBranches.setLayoutManager(new LinearLayoutManager(this));
        adapter = new BranchDtoAdapter(this);
        rvBranches.setAdapter(adapter);
        fabRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findLocation(true);
            }
        });
        findLocation(false);
    }

    private void findLocation(final boolean refreshOnly) {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (Utils.isNetworkLocationProviderEnabled(this)) {
            myLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        if (myLocation == null && Utils.isGPSEnabled(this)) {
            myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (myLocation == null) {
            myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        }
        if (myLocation == null) {
            Snackbar.make(coordinatorLayout, "Please turn on your GPS", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Dismiss", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
            return;
        }
        final ProgressDialog pd = ProgressDialog.show(this, "Getting stores", "Please wait...");
        API.getBranches(new API.GetBranchesSuccessListener() {
            @Override
            public void onSuccess(final List<BranchDto> branchDtos) {
                Collections.sort(branchDtos, new Comparator<BranchDto>() {
                    @Override
                    public int compare(BranchDto lhs, BranchDto rhs) {
                        float[] distResult1 = new float[3];
                        float[] distResult2 = new float[3];
                        Location.distanceBetween(myLocation.getLatitude(), myLocation.getLongitude(),
                                lhs.getLat(), lhs.getLng(), distResult1);
                        Location.distanceBetween(myLocation.getLatitude(), myLocation.getLongitude(),
                                rhs.getLat(), rhs.getLng(), distResult2);
                        Float d1 = distResult1[0];
                        Float d2 = distResult2[0];
                        lhs.setDist(d1);
                        rhs.setDist(d2);
                        return d1.compareTo(d2);
                    }
                });
                adapter.setBranchDtoList(branchDtos.subList(0, 10));
                if (refreshOnly) {
                    Snackbar.make(coordinatorLayout, "Location updated", Snackbar.LENGTH_LONG)
                            .setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            }).show();
                }
                pd.dismiss();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        DialogFragment df = AccountDialogFragment.newInstance();
        df.show(getSupportFragmentManager().beginTransaction(), null);
        return super.onOptionsItemSelected(item);
    }
}
