package com.spanky.smartapipoc.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.spanky.smartapipoc.App;
import com.spanky.smartapipoc.R;
import com.spanky.smartapipoc.adapter.PromoAdapter;
import com.spanky.smartapipoc.api.API;
import com.spanky.smartapipoc.dto.BranchDto;
import com.spanky.smartapipoc.dto.Promo;
import com.spanky.smartapipoc.fragment.AccountDialogFragment;
import com.truebanana.toast.Toast;

import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import ph.com.smart.consentSDK.billing.BillingError;
import ph.com.smart.consentSDK.billing.ChargeRequest;
import ph.com.smart.consentSDK.billing.ChargeResponse;
import ph.com.smart.consentSDK.consent.ConsentResponse;
import ph.com.smart.consentSDK.consent.PinConsentRequest;
import ph.com.smart.consentSDK.utils.Msisdn;
import ph.com.smart.consentSDK.utils.ResourceState;

/**
 * Created by Viel on 9/24/2015.
 */
public class DetailActivity extends BaseActivity {

    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.rvPromos)
    RecyclerView rvPromos;

    private boolean checkoutHidden;
    private PromoAdapter adapter;
    private List<Promo> selectedPromos;
    private float totalPrice;
    private String selection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        BranchDto branch = EventBus.getDefault().removeStickyEvent(BranchDto.class);
        initToolbar(branch.getBranchName());
        enableNavigationBackPressed(this);
        restoreStatusBarColor();
        rvPromos.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PromoAdapter(this, new RefreshOptionsMenuListener() {
            @Override
            public void refresh(boolean showCondition) {
                checkoutHidden = showCondition;
                invalidateOptionsMenu();
            }
        });
        rvPromos.setAdapter(adapter);
        EventBus.getDefault().register(this);
    }

    public interface RefreshOptionsMenuListener {
        void refresh(boolean showCondition);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_checkout);
        item.setVisible(checkoutHidden);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        adapter.getPromos();
        if (App.getSubscriber() != null) {
            new AlertDialog.Builder(this)
                    .setTitle("Are you sure you want to purchase the following?")
                    .setMessage((selection + "\nTOTAL: PHP " + totalPrice))
                    .setNegativeButton("Cancel", null)
                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PinConsentRequest pcr = new PinConsentRequest.Builder("CLIENT_ID", "CLIENT_SECRET")
                                    .setClientReferenceNumber("00000")
                                    .setScope("chargeAmountAirtime")
                                    .setSubscriberMsisdn(new Msisdn(App.getSubscriber()))
                                    .build();
                            API.sendPinConsent(pcr, new API.SendPinConsentListener() {
                                @Override
                                public void onSuccess(ConsentResponse consentResponse) {
                                    Snackbar.make(coordinatorLayout, consentResponse.getResponseDescription(), Snackbar.LENGTH_LONG)
                                            .setAction("Dismiss", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                }
                                            }).show();
                                    promtForPin();
                                }
                            });
                        }
                    })
                    .show();
        } else {
            Snackbar.make(coordinatorLayout, "Please register an account first!", Snackbar.LENGTH_LONG)
                    .setAction("Register", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DialogFragment df = AccountDialogFragment.newInstance();
                            df.show(getSupportFragmentManager().beginTransaction(), null);
                        }
                    }).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onEvent(List<Promo> promos) {
        selectedPromos = new LinkedList<>();
        selection = "";
        totalPrice = 0f;
        for (Promo p : promos) {
            if (p.isSelected()) {
                selectedPromos.add(p);
                totalPrice += p.getPrice();
                selection += p.getName() + "\n";
            }
        }
    }

    public void promtForPin() {
        final EditText pinEntry = new EditText(this);
        pinEntry.setInputType(InputType.TYPE_CLASS_NUMBER);
        pinEntry.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        new AlertDialog.Builder(this)
                .setTitle("Enter Pin")
                .setView(pinEntry)
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ChargeRequest cr = new ChargeRequest.Builder("CLIENT_ID", pinEntry.getText().toString(), new Msisdn(App.getSubscriber()))
                                .setParams("MERCHANT_ID", "00000", "SERVICE_ID", ResourceState.Amount.STATE_CHARGED)
                                .build();
                        API.sendChargeRequest(cr, new API.SendChargeRequestListener() {
                            @Override
                            public void onSuccess(ChargeResponse response) {
                                startActivity(new Intent(DetailActivity.this, TxnSummaryActivity.class));
                                EventBus.getDefault().postSticky(response);
                                finish();
                            }

                            @Override
                            public void onFailure(BillingError error) {
                                Snackbar.make(coordinatorLayout, error.getErrorMessage() + ": " +
                                        error.getErrorDescription(), Snackbar.LENGTH_LONG)
                                        .setAction("Dismiss", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                            }
                                        }).show();
                            }
                        });
                    }
                })
                .show();
    }
}
