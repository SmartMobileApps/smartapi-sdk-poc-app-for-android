package com.spanky.smartapipoc.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.spanky.smartapipoc.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import ph.com.smart.consentSDK.billing.ChargeResponse;

/**
 * Created by Viel on 9/29/2015.
 */
public class TxnSummaryActivity extends BaseActivity {

    @Bind(R.id.etTxnNumber)
    EditText etTxnNumber;
    @Bind(R.id.etRefCode)
    EditText etRefCode;
    @Bind(R.id.etAmount)
    EditText etAmount;
    @Bind(R.id.etDescription)
    EditText etDescription;
    @Bind(R.id.etStatus)
    EditText etStatus;
    @Bind(R.id.etResponseDesc)
    EditText etResponseDesc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_txn_summary);
        ButterKnife.bind(this);
        initToolbar("Transaction Summary");
        restoreStatusBarColor();
        enableNavigationBackPressed(this, R.drawable.abc_ic_clear_mtrl_alpha);
        ChargeResponse response = EventBus.getDefault().removeStickyEvent(ChargeResponse.class);

        etTxnNumber.setText(response.getServerReferenceCode());
        etRefCode.setText(response.getReferenceCode());
        etAmount.setText(String.format("PHP %.2f", response.getAmount()));
        etDescription.setText(response.getDescription());
        etStatus.setText(response.getTransactionOperationStatus());
        etResponseDesc.setText(response.getResponseDesc());
    }


}
