package com.spanky.smartapipoc.activity;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.spanky.smartapipoc.R;

import butterknife.Bind;

/**
 * Created by Viel on 9/24/2015.
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Bind(R.id.appToolbar)
    Toolbar toolbar;

    protected void initToolbar(String title) {
        if (toolbar == null) return;
        if (title == null) {
            setSupportActionBar(toolbar);
        } else {
            toolbar.setTitle(title);
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setElevation(getResources().getDimension(R.dimen.toolbar_elevation));
    }

    protected void enableNavigationBackPressed(final AppCompatActivity activity) {
        this.enableNavigationBackPressed(activity, 0);
    }

    protected void enableNavigationBackPressed(final AppCompatActivity activity, int iconResId) {
        if (iconResId == 0) {
            toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        } else {
            toolbar.setNavigationIcon(iconResId);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
    }

    protected void restoreStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }
    }

}
