package com.spanky.smartapipoc;

import android.app.Application;
import android.content.Context;

import com.truebanana.log.Log;

import ph.com.smart.consentSDK.utils.Msisdn;

/**
 * Created by Viel on 9/24/2015.
 */
public class App extends Application {

    private static final String PREFS_NAME = "P";
    private static final String PREFS_MSISDN = "M";

    private static Context context;

    public App() {
        context = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.setDebuggable(true);
    }

    private static void saveData(String key, String value) {
        context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit().putString(key, value).commit();
    }

    public static String getSubscriber() {
        return context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).getString(PREFS_MSISDN, null);
    }

    public static void saveSubscriber(Msisdn msisdn) {
        saveData(PREFS_MSISDN, msisdn.toString());
    }
}
